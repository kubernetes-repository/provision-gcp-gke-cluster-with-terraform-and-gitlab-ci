# About the Project
## The project to design a CICD pipeline to auto provision the GCP GKE cluster as Code using Terraform
*  Terraform configuration file (*.tf) is stored on GitLab Repo
  - backend.tf defines where Terraform stores its state data file
 - gke.tf provisions a GKE cluster and a separately managed node pool 
 - main.tf sets the Terraform version and provider information
 - outputs.tf returns the values of region, project_id, kubernetes_cluster_name, and kubernetes_cluster_host
 - terraform.tfvars is a template for the project_id and region variables
 - vpc.tf provisions a VPC and subnet. A new VPC is created for this tutorial so it doesn't impact your existing cloud environment and resources. 
*  Terraform uses state files to store details about the infrastructure configuration. In this project, I use Google Cloud Storage as Terraform backend to securely store the state file. Note that, GCS supports state locking to to prevents others from acquiring the lock and potentially corrupting your state

## Prerequisite
* For this tutorial, you will need a GCP account, and create following resources:
  - A GCP Project: GCP organizes resources into projects. Create one now in the GCP console and make note of the project ID. You can see a list of your projects in the cloud resource manager.
  - Google Compute Engine: Enable Google Compute Engine for your project in the GCP console. Make sure to select the project you are using to follow this tutorial and click the "Enable" button.
  - A GCP service account key: Create a service account key with Editor role to enable Terraform to access your GCP account. You can securely store the service account key using Gitlab variable under Settings > CI/CD > Variables.
  ![image](/uploads/655518698708535b8d82096b9d790ed6/image.png)
  - A GCS to store the Terraform state file
  ![image](/uploads/55a0486d88cb8993d1fc1a45322e3b12/image.png)

## The CICD Pipeline
* CI/CD Pipeline: GitLab Repo ----[trigger]---> Gitlab Runner(default)---[pull]-->Terraform(image)---[deploy]---> Google Cloud Platform

  ![image](/uploads/922760e743033af49bca6f2bc124547d/image.png)
  
* Pipeline stages
  - Validate: validates the Terraform configuration files pulled from GitLab repo
  - Plan: creates an execution plan, which lets you preview the changes that Terraform plans to make to your infrastructure
  - Apply: executes the actions proposed in the plan stage.
  - Destroy: destroy all GCP resources managed by the Terraform configuration

## Notification
* To monitor the events for the pipelines, go to Project > Settings > Integrations, then add Slack notification


* GitLab will send the notification to Slack when git commit, push,... or pipeline status changes


