terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  credentials = "terraform-key.json"
  project = var.project_id
  region  = var.region
}


