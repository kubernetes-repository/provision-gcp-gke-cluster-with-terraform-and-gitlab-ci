terraform {
    required_version = ">=1.2.5"
    backend "gcs" {
         credentials = "terraform-key.json"
         bucket      = "terraform_states_bucket"
    }
}
